# Josephus Problem

You are one of the soldiers of Josephus. You are trapped in a cave surrounded by Romans, who want to capture Josephus and his soldiers. Josephus decides that all the men will kill each other and last one remaining will kill themselves instead of getting captured.

The killing will be done as follows:
1. Everyone will be seated in a circle.
2. First person will kill the person to his right with his sword.
3. The sword is then passed to the next person alive.
4. This process is repeated until only one person is alive.
5. The last person kills himself and completes the mass suicide.

[![Josephus Problem](https://static.wixstatic.com/media/679bcb_e8e1a23e07a04c67b92a9bb34e86e723~mv2.gif)](https://static.wixstatic.com/media/679bcb_e8e1a23e07a04c67b92a9bb34e86e723~mv2.gif "Josephus Problem")

## Problem Statement

You are not willing to kill yourself and would prefer to be captured by the Romans. So you plan to be the last survivor and would surrender to the Romans.

Consider that there are N soldiers in the circle, the problem is to find the right position in the circle to be seated so that you end up being the last person alive.

## Sample I/O
### Sample Input 1:
`5`
### Sample Output 1:
`3`
### Explanation:
Soldiers: 1 2 3 4 5
1. 1 kills 2, passes the sword to 3. (1 ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) `3` 4 5)
2. 3 kills 4, passes the sword to 4. (1 ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) 3 ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) `5`)
3. 5 kills 1, passes the sword to 3. (![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) `3` ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) 5)
3. 3 kills 5. (![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) `3` ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+) ![#f03c15](https://via.placeholder.com/15/f03c15/000000?text=+))

Soldier 3 is the last one alive.
