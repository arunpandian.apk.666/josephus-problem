import getSafePosition from './solution/index.js';

const noOfSoldiers = 5;

console.log('Brute Force Solution:');
let safePosition = getSafePosition(noOfSoldiers, 0);
safePosition ? console.log("Josephus is save in", safePosition) : console.log('No of soldiers cannot be less than 1');

console.log('\nDivide and Conquer Solution:');
safePosition = getSafePosition(noOfSoldiers, 1);
safePosition ? console.log("Josephus is save in", safePosition) : console.log('No of soldiers cannot be less than 1');

console.log('\nAnalytical Solution:');
safePosition = getSafePosition(noOfSoldiers, 2);
safePosition ? console.log("Josephus is save in", safePosition) : console.log('No of soldiers cannot be less than 1');