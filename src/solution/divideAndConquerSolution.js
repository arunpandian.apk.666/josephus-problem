function findLargestOrderOfTwoBelow(noOfSoldiers) {
    let largestOrder = 1;
    while(largestOrder <= noOfSoldiers) {
        largestOrder *= 2;
    }
    return largestOrder / 2;
}

function findSafePositionWith(noOfSoldiers) {
    const largestOrderOfTwo = findLargestOrderOfTwoBelow(noOfSoldiers);
    const difference = noOfSoldiers - largestOrderOfTwo;
    return difference * 2 + 1;
}

export default findSafePositionWith;