function convertIntegerToBinaryString(integerValue) {
    let binaryString = '';
    while(integerValue >= 1) {
        binaryString += integerValue % 2;
        integerValue = Math.floor(integerValue / 2);
    }
    return binaryString.split('').reverse().join('');
}

function rotateStringLeft(stringValue) {
    return stringValue.substr(1, stringValue.length) + stringValue[0];
}

function convertBinaryToDecimal(binaryString) {
    let multiplier = 1;
    let decimalValue = 0;
    const stringToConvert = binaryString.split('').reverse().join('');
    for(let i = 0; i < stringToConvert.length; i++) {
        decimalValue = decimalValue + stringToConvert[i] * multiplier;
        multiplier *= 2;
    }
    return decimalValue;
}

function findSafePositionWith(noOfSoldiers) {
    const binaryString = convertIntegerToBinaryString(noOfSoldiers);
    const rotatedBinaryString = rotateStringLeft(binaryString);
    return convertBinaryToDecimal(rotatedBinaryString);
}

export default findSafePositionWith;