import bruteForceSolution from './bruteForceSolution.js';
import divideAndConquerSolution from "./divideAndConquerSolution.js";
import analyticalSolution from "./analyticalSolution.js";

const solutions = [
    bruteForceSolution,
    divideAndConquerSolution,
    analyticalSolution
]

function getSafePosition(noOfSoldiers, solutionType) {
    if(noOfSoldiers <= 0) {
        return;
    }
    return solutions[solutionType](noOfSoldiers);
}

export default getSafePosition;