function isLastPersonAlive(soldiers) {
    let noOfAliveSoldiers = 0;
    for(let i=0; i < soldiers.length; i++) {
        if(soldiers[i] === 1) {
            noOfAliveSoldiers++;
        }
    }
    return noOfAliveSoldiers === 1;
}

function findNextAliveSolider(soldiers, currentSolider) {
    let i = (currentSolider + 1) % soldiers.length;
    while(true) {
        if(soldiers[i] === 1) {
            return i;
        }
        i = (i + 1) % soldiers.length;
    }
}

function killSoldier(soldiers, soldierId) {
    soldiers[soldierId] = -1;
    return soldiers;
}

function findSafePositionWith(noOfSoldiers) {
    let soldiers = new Array(noOfSoldiers).fill(1);
    let i = 0;
    while(true) {
        if(isLastPersonAlive(soldiers)) {
            return i + 1;
        }
        soldiers = killSoldier(soldiers, findNextAliveSolider(soldiers, i));
        i = findNextAliveSolider(soldiers, i);
    }
}

export default findSafePositionWith;