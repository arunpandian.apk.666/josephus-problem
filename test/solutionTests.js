import getSafePosition from "../src/solution/index.js";
import chai from 'chai';
const expect = chai.expect

describe("Validation Tests", () => {

    it("should return 0 when the no of soldiers is invalid (less than or equal to 0)", ()=> {
        expect(getSafePosition(-1, 0)).to.equals(0);
    })

    it("should return a valid position when the no of soldiers is valid number", () => {
        expect(getSafePosition(41, 0)).to.equals(19);
    })

    it("should always return a position number lesser than or equal to no of soldiers", ()=> {
        expect(getSafePosition(5, 0)).to.lessThan(5);
    })
})