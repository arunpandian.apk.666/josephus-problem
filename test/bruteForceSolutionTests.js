import bruteForceSolution from "../src/solution/bruteForceSolution.js";
import chai from 'chai';
const expect = chai.expect

describe("Brute Force Solution Tests", () => {

    it("should return 1 when the no of soldiers is 1", ()=> {
        expect(bruteForceSolution(1)).to.equals(1);
    })

    it("should return 27 when the no of soldiers is 77", () => {
        expect(bruteForceSolution(77)).to.equals(27);
    })
})