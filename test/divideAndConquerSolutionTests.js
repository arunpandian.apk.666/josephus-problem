import divideAndConquerSolution from "../src/solution/divideAndConquerSolution.js";
import chai from 'chai';
const expect = chai.expect

describe("Divide and Conquer Solution Tests", () => {

    it("should return 1 when the no of soldiers is 1", ()=> {
        expect(divideAndConquerSolution(1)).to.equals(1);
    })

    it("should return 1 when the no of soldiers is a power of 2", ()=> {
        expect(divideAndConquerSolution(8)).to.equals(1);
    })

    it("should return 19 when the no of soldiers is 41", () => {
        expect(divideAndConquerSolution(41)).to.equals(19);
    })
})