import analyticalSolution from "../src/solution/analyticalSolution.js";
import chai from 'chai';
const expect = chai.expect

describe("Analytical Solution Tests", () => {

    it("should return 1 when the no of soldiers is 1", ()=> {
        expect(analyticalSolution(1)).to.equals(1);
    })

    it("should return 19 when the no of soldiers is 41", () => {
        expect(analyticalSolution(41)).to.equals(19);
    })
})